function tableCreate() 
{
    var columns = parseInt(document.getElementById('columnsNumber').value);
    var rows = parseInt(document.getElementById('rowsNumber').value);
    var body = document.getElementsByTagName('body')[0];
    var tbl = document.createElement('table');
    tbl.setAttribute('border', '');

    
    
    for (var i = 0; i < rows; i++) 
    {
        var tr = tbl.insertRow(i);
        for (var j = 0; j < columns; j++) 
        {
                var td = tr.insertCell(j);
                td.addEventListener("click", function() {cellIsClicked(this);});
                td.textContent = (i + 1).toString() + (j + 1);
        }
    }
    
    body.getElementsByTagName('table').length ? body.replaceChild(tbl, body.getElementsByTagName('table')[0]) : body.appendChild(tbl);                                                                        
}

function cellIsClicked(e)
{
    if(e.style.backgroundColor != "red")
        e.style.backgroundColor = "red";
    else
        e.style.backgroundColor = "";
}